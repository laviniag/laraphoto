<?php

namespace Tests\Unit;

use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function index ()
    {
        $wiki_factory = new WikiTitle();
        $html_factory = new HtmlTitle();

        $this->test1($wiki_factory);
        $this->test1($html_factory);

        $this->test2($wiki_factory);
        $this->test2($html_factory);
    }

    function test1(AbstractTitle $factory)
    {
        $title = $factory->createTitle();
        $title->set_content('Prova');
        echo $title->render();
    }

    function test2(AbstractTitle $factory)
    {
        $factory->set_content('Factory');
        echo $factory->render();
    }
}


interface AbstractTitle
{
    public function createTitle();
    public function set_content($content);
    public function render();
}

class HtmlTitle implements AbstractTitle
{
    private $title;

    public function createTitle()
    {
        return new HtmlTitle();
    }

    public function set_content($content)
    {
        $this->title = $content;
    }

    public function render()
    {
        return "<h1>".$this->title."</h1>";
    }
}

class WikiTitle implements AbstractTitle
{
    private $title;

    public function createTitle()
    {
        return new WikiTitle();
    }

    public function set_content($content)
    {
        $this->title = $content;
    }

    public function render()
    {
        return "= ".$this->title." =";
    }
}
