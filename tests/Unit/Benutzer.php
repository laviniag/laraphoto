<?php

namespace Tests\Unit;

use App\Http\Controllers\Controller;

class BenutzerController extends Controller
{
    public function index ()
    {
        $um = new BenutzersManager(array(
            'brad' => 'admin',
            'kate' => 'reader',
            'hulk' => 'writer'
        ));

        $benutzers = array('brad', 'kate', 'hulk');
        foreach ($benutzers as $benutzer) {
            $u = $um->create_Benutzer($benutzer);
            if ($u->can_read())
                echo $u->name . " può leggere <br />";
            if ($u->can_write())
                echo $u->name . " può scrivere <br />";
            if ($u->can_execute())
                echo $u->name . " può eseguire <br />";
        }
    }
}

interface IBenutzersManager
{
    public function create_Benutzer($type);
}

class BenutzersManager implements IBenutzersManager
{
    private $roles;
    private static $klasse = ['admin' => Admin::class,
        'reader' => Reader::class,
        'writer' => Writer::class,
        'executor' => Executor::class];

    public function __construct($roles)
    {
        $this->roles = $roles;
    }

    public function create_Benutzer($name)
    {
        if(array_key_exists($name, $this->roles))
        {
            $role = $this->roles[$name];
            $class = self::$klasse[strtolower($role)];
            return new $class($name);
        }

        return new DefaultBenutzer($name);
    }
}

abstract class Benutzer
{
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function can_write()
    {
        return false;
    }

    public function can_read()
    {
        return false;
    }

    public function can_execute()
    {
        return false;
    }
}

class DefaultBenutzer extends Benutzer
{
}

class Admin extends Benutzer
{
    public function can_write()
    {
        return true;
    }

    public function can_read()
    {
        return true;
    }

    public function can_execute()
    {
        return true;
    }
}

class Reader extends Benutzer
{
    public function can_read()
    {
        return true;
    }
}

class Writer extends Benutzer
{
    public function can_write()
    {
        return true;
    }
}

class Executor extends Benutzer
{
    public function can_execute()
    {
        return true;
    }
}

