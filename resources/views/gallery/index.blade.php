@extends('layouts/main')

@section('content_header')
    @if(Session::has('message'))
        <div class="alert alert-info">
            {{ Session::get('message') }}
        </div>
    @endif
@stop

@section('callout')
    <h3>GALLERY LIST</h3>
@stop

@section('container')
    <div class="row small-up-2 medium-up-3 large-up-4">
        <div class="column">
            <img class="thumbnail" src="http://placehold.it/550x550">
            <h5>My Site</h5>
        </div>
        <div class="column">
            <img class="thumbnail" src="http://placehold.it/550x550">
            <h5>My Site</h5>
        </div>
        <div class="column">
            <img class="thumbnail" src="http://placehold.it/550x550">
            <h5>My Site</h5>
        </div>
        <div class="column">
            <img class="thumbnail" src="http://placehold.it/550x550">
            <h5>My Site</h5>
        </div>
        <div class="column">
            <img class="thumbnail" src="http://placehold.it/550x550">
            <h5>My Site</h5>
        </div>
        <div class="column">
            <img class="thumbnail" src="http://placehold.it/550x550">
            <h5>My Site</h5>
        </div>
        <div class="column">
            <img class="thumbnail" src="http://placehold.it/550x550">
            <h5>My Site</h5>
        </div>
        <div class="column">
            <img class="thumbnail" src="http://placehold.it/550x550">
            <h5>My Site</h5>
        </div>
        <div class="column">
            <img class="thumbnail" src="http://placehold.it/550x550">
            <h5>My Site</h5>
        </div>
        <div class="column">
            <img class="thumbnail" src="http://placehold.it/550x550">
            <h5>My Site</h5>
        </div>
        <div class="column">
            <img class="thumbnail" src="http://placehold.it/550x550">
            <h5>My Site</h5>
        </div>
        <div class="column">
            <img class="thumbnail" src="http://placehold.it/550x550">
            <h5>My Site</h5>
        </div>
    </div>
@stop