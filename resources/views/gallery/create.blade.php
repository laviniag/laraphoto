@extends('layouts/main')

@section('callout')
    <h3>Create Gallery</h3>
    <p>Create a new gallery and start uploading</p>
@stop

@section('container')
    {!! Form::open(['action' => 'GalleryController@store', 'files'=>'true']); !!}
    {!! Form::label('name', 'Name'); !!}
    {!! Form::text('name', $value=null, $attributes = ['placeholder' => 'Gallery Name']); !!}
    {!! Form::label('description', 'Description'); !!}
    {!! Form::text('description', $value=null, $attributes = ['placeholder' => 'Description']); !!}
    {!! Form::label('cover_image', 'Cover Image'); !!}
    {!! Form::file('cover_image', $attributes = []); !!}

    {!! Form::submit('CREATE', $attributes = ['class' => 'button']); !!}
    {!! Form::close(); !!}
@stop