@extends('layouts/dashboard')

@section('csslink')
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/foundation.min.css') }}" />
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>
    </script>
@stop

@section('content')
    <div class="off-canvas-wrapper">
        <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

            <div class="off-canvas-content" data-off-canvas-content>
                 <div class="callout primary">
                    <div class="row column">
                        @yield('callout')
                    </div>
                </div>
                <br>

                @yield('container')
                <hr>
            </div>
        </div>
    </div>

    <div class="footer">
        <p>All copyright reserved &copy;</p>
    </div>
@stop

@section('jscustom')
    <!-- Scripts -->
{{--    <script type="text/javascript" src="{{ asset('js/vendor/jquery.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('js/vendor/foundation.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>--}}

    <script>
        $(document).foundation();
    </script>
@stop
