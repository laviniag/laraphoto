<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class GalleryController extends Controller
{
    /**
     * List Galleries
     */
    public function index ()
    {
        return view('gallery/index');
    }

    /**
     * Create form Gallery
     */
    public function create ()
    {
        return view('gallery/create');
    }

    /**
     * Store a Gallery
     */
    public function store (Request $request)
    {
//        $gallery = array();
//        $gallery['name'] = $request->input('name');
//        $gallery['description'] = $request->input('description');
//        $gallery['owner_id'] = 1;
        $gallery = new Gallery();
        $gallery->setName($request->name);
        $gallery->setDescription($request->description);
        $gallery->setOwnerId(1);

        $cover_image = $request->file('cover_image');

        if($cover_image) {
            $image_file = $cover_image->getClientOriginalName();
            $cover_image->move(public_path('images'), $image_file);
            $gallery->setCoverImage($image_file);
        } else {
            $gallery->setCoverImage('noimage.jpg');
        }
        // Persist in database
//        DB::table('galleries')->insert($gallery);
        $gallery->save();

        return Redirect::route('gallery.index')->with('message', "Gallery created !");
    }

    /**
     * Show a Gallery
     */
    public function show ($id)
    {
        die('GALLERY SHOW '.$id);
    }

    /**
     * Edit a Gallery
     */
    public function edit ($id)
    {
        die('GALLERY EDIT');
    }

    /**
     * Update a Gallery
     */
    public function update (Request $request)
    {
    }

    /**
     * Delete a Gallery
     */
    public function destroy ($id)
    {
        die('GALLERY DESTROY');
    }

}
