<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PhotoController extends Controller
{
    /**
     * List Photos
     */
    public function index ()
    {
        die('PHOTO INDEX');
    }

    /**
     * Create form Photo
     */
    public function create ()
    {
        die('PHOTO CREATE');
    }

    /**
     * Store a Photo
     */
    public function store (Request $request)
    {
    }

    /**
     * Show a Photo
     */
    public function show ($id)
    {
        die('PHOTO SHOW '.$id);
    }

    /**
     * Show Photo details
     */
    public function details ($id)
    {
        die('PHOTO DETAIL '.$id);
    }

    /**
     * Edit a Photo
     */
    public function edit ($id)
    {
        die('PHOTO EDIT');
    }

    /**
     * Update a Photo
     */
    public function update (Request $request)
    {
    }

    /**
     * Delete a Photo
     */
    public function destroy ($id)
    {
        die('PHOTO DESTROY');
    }

}
